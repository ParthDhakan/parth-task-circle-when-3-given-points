package com.example.parthstaskcircle

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import android.widget.FrameLayout

import android.widget.LinearLayout
import androidx.core.content.ContextCompat

import kotlin.math.sqrt


class MainActivity : AppCompatActivity() {

    private var touchCount = 0
    private lateinit var clRoot: FrameLayout
    private var listPT: MutableList<PT> = ArrayList()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        clRoot = findViewById(R.id.clRoot)
        clRoot.setOnTouchListener { v, event ->
            when (event?.action) {
                else -> {
                    if(touchCount==3){
                        touchCount = 0
                        listPT.clear()
                        clRoot.removeAllViews()
                        clRoot.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
                    }
                    if (touchCount < 3) {
                        val inflatedView = View.inflate(this, R.layout.item_circle, null)
                        val params = LinearLayout.LayoutParams(20, 20)
                        clRoot.addView(inflatedView, params)
                        inflatedView.animate().x(event.x).y(event.y)
                        touchCount++

                        listPT.add(PT(event.x, event.y))


                        if (touchCount == 3) {
                            val centerPT = getCircleCenter(listPT[0], listPT[1], listPT[2])
                            val bitmap = Bitmap.createBitmap(
                                clRoot.width,
                                clRoot.height,
                                Bitmap.Config.ARGB_8888
                            )
                            val canvas = Canvas(bitmap)
                            val paint = Paint()
                            paint.color = Color.parseColor("#000000")
                            paint.strokeWidth = 20F
                            paint.style = Paint.Style.STROKE
                            paint.isAntiAlias = true
                            paint.isDither = true
                            canvas.drawCircle(centerPT.x, centerPT.y, getRadius(centerPT,listPT), paint)
                            clRoot.background = BitmapDrawable(resources, bitmap)
                        }
                    }
                }
            }

            v?.onTouchEvent(event) ?: false
        }
    }

    private fun getCircleCenter(A: PT, B: PT, C: PT): PT {
        val yDeltaA: Float = B.y - A.y
        val xDeltaA: Float = B.x - A.x
        val yDeltaB: Float = C.y - B.y
        val xDeltaB: Float = C.x - B.x
        val center = PT(0F, 0F)
        val aSlope = yDeltaA / xDeltaA
        val bSlope = yDeltaB / xDeltaB
        center.x = (aSlope * bSlope * (A.y - C.y) + bSlope * (A.x + B.x)
                - aSlope * (B.x + C.x)) / (2 * (bSlope - aSlope))
        center.y = -1 * (center.x - (A.x + B.x) / 2) / aSlope + (A.y + B.y) / 2
        return center
    }

    private fun getRadius(centerPoint: PT, listPT: MutableList<PT>): Float {
        var maxDistance: Float
        val distance1 =
            sqrt((centerPoint.x - listPT[0].x) * (centerPoint.x - listPT[0].x) + (centerPoint.y - listPT[0].y) * (centerPoint.y - listPT[0].y))
        val distance2 =
            sqrt((centerPoint.x - listPT[0].x) * (centerPoint.x - listPT[0].x) + (centerPoint.y - listPT[0].y) * (centerPoint.y - listPT[0].y))
        val distance3 =
            sqrt((centerPoint.x - listPT[0].x) * (centerPoint.x - listPT[0].x) + (centerPoint.y - listPT[0].y) * (centerPoint.y - listPT[0].y))
        maxDistance = distance1.coerceAtLeast(distance2)
        maxDistance= maxDistance.coerceAtLeast(distance3)
        return maxDistance
    }
}